import requests  
import re  
import sys
import os

def list_files(path, file_ending):
    files = []
    for file in os.listdir(path):
        if file.endswith(file_ending):
            files.append(file)
    return files

def read_file_content_into_dictionary(file):
    file_content = {}
    with open(file, "r") as f:
        for line in f:
            line_content = line.rstrip().split(" ")
            key = line_content[0]
            # C++ does not allow number enums
            if(key[0].isdigit()):
                key = "number_" +key
            # QML only allows uppercase enums
            key = key.capitalize()
            value = line_content[1]
            file_content[str(key)] = value
    return file_content

if __name__ == '__main__':
    path = os.getcwd()
    codepoints = list_files(path, '.codepoints')

    for codepoint in codepoints:
        file_content = read_file_content_into_dictionary(os.path.join(path,codepoint))
        name = codepoint.split("[")[0]
        file_complete  = "\nenum class "
        file_complete  +=  name
        file_complete  +=  " {\n"
        for key in file_content:
            #file_complete  += "\t" + key + " = \"\\\\u" + file_content[key] + "\",\n"
            file_complete  += "\t" + key + " = 0x" + file_content[key] + ",\n"
        file_complete += "};\n"
        file_complete += "\nQ_ENUM(" + name +")"
        file_complete += "\n\n"

        with open(str(name)+'_generated.h', 'w') as outfile:
            outfile.write(file_complete)
