import QtQuick
import QtQuick.Controls
import "MaterialSymbolsSharp_generated.js" as MaterialSymbols

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")


    FontLoader {
        id: webFontSharp
        source: "MaterialSymbolsSharp[FILL,GRAD,opsz,wght].ttf"
    }
    FontLoader {
        id: webFontOutlined
        source: "MaterialSymbolsRounded[FILL,GRAD,opsz,wght].ttf"
    }
    FontLoader {
        id: webFontRounded
        source: "MaterialSymbolsOutlined[FILL,GRAD,opsz,wght].ttf"
    }
    Row {
        anchors.fill: parent
        Text {
            text: MaterialSymbols.Icons.Delete
            font.pointSize: 21
            font.family: webFontSharp.name
        }
        Text {
            text: MaterialSymbols.Icons.Delete
            font.pointSize: 21
            font.family: webFontOutlined.name
        }
        Text {
            text: MaterialSymbols.Icons.Delete
            font.pointSize: 21
            font.family: webFontRounded.name
        }
    }
}
