#pragma once
#include <QObject>
#include <QQmlEngine>

class MaterialSymbols : public QObject {
  Q_OBJECT
  QML_ELEMENT
  QML_SINGLETON
public:
  explicit MaterialSymbols(QObject *parent = nullptr);
#include "MaterialSymbolsOutlined_generated.h"
};
